﻿CREATE TABLE [dbo].[Schedule] (
    [Id]               BIGINT         IDENTITY (1, 1) NOT NULL,
    [EventId]          BIGINT         NOT NULL,
    [StartingDateTime] DATETIME       NULL,
    [EndingDateTime]   DATETIME       NULL,
    [Remarks]          NVARCHAR (MAX) NULL,
    [Status]           INT            NOT NULL,
    [AddedDateTime]    DATETIME       NOT NULL,
    [AddedBy]          BIGINT         NOT NULL,
    [UpdatedDateTime]  DATETIME       NULL,
    [UpdatedBy]        BIGINT         NULL,
    CONSTRAINT [PK_dbo.Schedule] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Schedule_dbo.Event_EventId] FOREIGN KEY ([EventId]) REFERENCES [dbo].[Event] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.Schedule_dbo.User_AddedBy] FOREIGN KEY ([AddedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_dbo.Schedule_dbo.User_UpdatedBy] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[User] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_EventId]
    ON [dbo].[Schedule]([EventId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AddedBy]
    ON [dbo].[Schedule]([AddedBy] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UpdatedBy]
    ON [dbo].[Schedule]([UpdatedBy] ASC);

