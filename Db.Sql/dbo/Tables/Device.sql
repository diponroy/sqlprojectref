﻿CREATE TABLE [dbo].[Device] (
    [Id]              BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (150) NOT NULL,
    [Description]     NVARCHAR (300) NULL,
    [Status]          INT            NOT NULL,
    [AddedDateTime]   DATETIME       NOT NULL,
    [AddedBy]         BIGINT         NOT NULL,
    [UpdatedDateTime] DATETIME       NULL,
    [UpdatedBy]       BIGINT         NULL,
    CONSTRAINT [PK_dbo.Device] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Device_dbo.User_AddedBy] FOREIGN KEY ([AddedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_dbo.Device_dbo.User_UpdatedBy] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[User] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_AddedBy]
    ON [dbo].[Device]([AddedBy] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UpdatedBy]
    ON [dbo].[Device]([UpdatedBy] ASC);

