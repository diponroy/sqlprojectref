﻿CREATE TABLE [dbo].[User] (
    [Id]              BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (150) NOT NULL,
    [ContactNo]       NVARCHAR (20)  NULL,
    [Email]           NVARCHAR (100) NOT NULL,
    [Password]        NVARCHAR (50)  NOT NULL,
    [Status]          INT            NOT NULL,
    [AddedDateTime]   DATETIME       NOT NULL,
    [AddedBy]         BIGINT         NOT NULL,
    [UpdatedDateTime] DATETIME       NULL,
    [UpdatedBy]       BIGINT         NULL,
    CONSTRAINT [PK_dbo.User] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.User_dbo.User_AddedBy] FOREIGN KEY ([AddedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_dbo.User_dbo.User_UpdatedBy] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[User] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_AddedBy]
    ON [dbo].[User]([AddedBy] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UpdatedBy]
    ON [dbo].[User]([UpdatedBy] ASC);

