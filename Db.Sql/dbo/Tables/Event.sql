﻿CREATE TABLE [dbo].[Event] (
    [Id]              BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (150) NOT NULL,
    [Description]     NVARCHAR (300) NULL,
    [DeviceId]        BIGINT         NOT NULL,
    [Status]          INT            NOT NULL,
    [AddedDateTime]   DATETIME       NOT NULL,
    [AddedBy]         BIGINT         NOT NULL,
    [UpdatedDateTime] DATETIME       NULL,
    [UpdatedBy]       BIGINT         NULL,
    CONSTRAINT [PK_dbo.Event] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Event_dbo.Device_DeviceId] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[Device] ([Id]),
    CONSTRAINT [FK_dbo.Event_dbo.User_AddedBy] FOREIGN KEY ([AddedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_dbo.Event_dbo.User_UpdatedBy] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[User] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_DeviceId]
    ON [dbo].[Event]([DeviceId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AddedBy]
    ON [dbo].[Event]([AddedBy] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UpdatedBy]
    ON [dbo].[Event]([UpdatedBy] ASC);

