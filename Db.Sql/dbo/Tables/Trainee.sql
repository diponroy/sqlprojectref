﻿CREATE TABLE [dbo].[Trainee] (
    [Id]              BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (150) NOT NULL,
    [ContactNo]       NVARCHAR (20)  NULL,
    [Email]           NVARCHAR (100) NULL,
    [EventId]         BIGINT         NOT NULL,
    [Status]          INT            NOT NULL,
    [AddedDateTime]   DATETIME       NOT NULL,
    [AddedBy]         BIGINT         NOT NULL,
    [UpdatedDateTime] DATETIME       NULL,
    [UpdatedBy]       BIGINT         NULL,
    CONSTRAINT [PK_dbo.Trainee] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Trainee_dbo.Event_EventId] FOREIGN KEY ([EventId]) REFERENCES [dbo].[Event] ([Id]),
    CONSTRAINT [FK_dbo.Trainee_dbo.User_AddedBy] FOREIGN KEY ([AddedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_dbo.Trainee_dbo.User_UpdatedBy] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[User] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_EventId]
    ON [dbo].[Trainee]([EventId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AddedBy]
    ON [dbo].[Trainee]([AddedBy] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UpdatedBy]
    ON [dbo].[Trainee]([UpdatedBy] ASC);

